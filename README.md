# How to run it.

```
git clone ...
cd <project folder>
./run.sh
```

Script run.sh builds the projects and runs it locally with files from "data" folder.


# Output data

Output files are written into the same folder as input files.

# Assumptions

## About the data 
 
I assumed that corrupted lines are ignored and application continues with proper data.  

I found 3 cases with corrupted data:  
* 8,Peggy Dennis,f,4990.00 - department nr out of Departmens range  
* 6,Doreen Rodgers f,2060.00 - missing coma, invalid nr of columns  
* 7,Joel Scott,m,4470.0O - salary with letter "O"  

All are logged during read with WARN level.  
Next 3 WARNs for age files is just consequence of ignoring 3 former employee records.

Alternatively the app could treat the case 1 reading the employee without department info  
Then it's possible to calculate "average income by age ranges with factor of ten" considering this case.  

But in real case since nothing was stated about how to treat corrupted input data - better is to LOG it with WARN  
and then clarify the problematic cases with the data provider. Depending on case it may even appear that that if  
single data is corrupted safer is to not read data at all, but here I treat it less strict.  

## About the tests

Since libraries are limited only to Oracle Java Runtime I decided to use java.util.logging.Logger  
and because of Junit is also separate library I decided to test only the most important things.  

Usually I split the code into testable parts, most of the time writing tests before writing the code - in TDD approach. 