#!/bin/bash

echo "Rebuilding ..."
mvn clean package > /dev/null

echo "Running the app with data from ./data folder ..."
java -cp target/hubrick-backend-challenge-1.0-SNAPSHOT.jar  App data/departments.csv data/employees.csv data/ages.csv

echo "Output files should be in data folder:"
ls data/{income*,employee-age*} -la
