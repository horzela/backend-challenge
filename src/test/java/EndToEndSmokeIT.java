import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.logging.Logger;

/**
 * Simple smoke test.
 */
public class EndToEndSmokeIT {

    private static final Logger LOGGER = Logger.getLogger(EndToEndSmokeIT.class.getName());

    public static void main(String[] args) throws URISyntaxException, IOException {

        App.main(new String[]{"data/departments.csv", "data/employees.csv", "data/ages.csv"});

        assertFileExists("data/income-by-department.csv");
        assertFileExists("data/income-95-by-department.csv");
        assertFileExists("data/income-average-by-age-range.csv");
        assertFileExists("data/employee-age-by-department.csv");

        LOGGER.info("Test passed");

    }

    private static void assertFileExists(final String file) {
        if(!Paths.get(file).toFile().exists()) {
            throw new RuntimeException("Test failed - unable fo find expected file");
        }
    }

}
