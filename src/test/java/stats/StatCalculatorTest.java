package stats;

import java.util.logging.Logger;

/**
 * Tests for StatCalculator.
 */
public class StatCalculatorTest {

    private static final Logger LOGGER = Logger.getLogger(StatCalculatorTest.class.getName());

    // @Test :-(
    // Only libraries that are part of Oracle Java Runtime are allowed in production code.
    public static void main(String[] args) {
        final StatCalculatorTest test = new StatCalculatorTest();

        // run test cases:
        test.shouldCalculateMedianProperly();
        test.shouldCalculate95PercentileProperly();

        LOGGER.info("Test suite passed");
    }

    private void shouldCalculateMedianProperly() {
        // GIVEN
        final double[] inputValues = new double[]{22, 28, 28, 33, 35, 35, 36, 37, 38, 42, 45, 48, 70, 74, 80};

        // WHEN
        final double value = StatCalculator.calculateMedian(inputValues);

        // THEN
        assertEquals(value, 37, 0.01);
    }

    private void shouldCalculate95PercentileProperly() {
        // GIVEN
        final double[] inputValues = new double[]{11, 22, 33, 34, 35, 36, 37, 38, 39, 40, 40, 40, 40, 41, 41, 42, 42, 42, 43, 44, 44, 45};

        // WHEN
        final double value = StatCalculator.calculate95thPercentile(inputValues);

        // THEN
        assertEquals(value, 44.85, 0.01);
    }

    public static void assertEquals(final double value, final double expected, final double precision) {
        if(Math.abs(value - expected) > precision) {
            throw new TestFailedException("Value: " + value + " is not equal to expected: " + expected);
        }
    }

    private static class TestFailedException extends RuntimeException {
        public TestFailedException(final String msg) {
            super(msg);
        }
    }

}
