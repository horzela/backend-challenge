package model;

import java.math.BigDecimal;

/**
 * Employee data model.
 */
public class Employee {

    private final String name;
    private final BigDecimal income;
    private Integer age;

    /**
     * Constructor
     * @param name employee's name
     * @param income income
     */
    public Employee(final String name, final BigDecimal income) {
        this.name = name;
        this.income = income;
    }

    public String getName() {
        return name;
    }

    public void setAge(final Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Employee{");
        sb.append("name='").append(name).append('\'');
        sb.append(", income=").append(income);
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }

    public Integer getAge() {
        return age;
    }

    public BigDecimal getIncome() {
        return income;
    }
}
