package model;

import stats.StatCalculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * Department model.
 */
public class Department {

    private final String name;
    private List<Employee> employees = new ArrayList<>();

    public Department(final String name) {
        this.name = name;
    }

    public void add(final Employee employee) {
        this.employees.add(employee);
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }

    /**
     * Calculates the median income by department
     *
     * @return the median income by department
     */
    public BigDecimal calculateMedianIncome() {
        final double[] incomeCollection = getAllIncomes();
        return BigDecimal.valueOf(StatCalculator.calculateMedian(incomeCollection));
    }

    /**
     * Calculates the 95-percentile income by department
     *
     * @return the 95-percentile income by department
     */
    public BigDecimal calculate95PercentileIncome() {
        final double[] incomeCollection = getAllIncomes();
        return BigDecimal.valueOf(StatCalculator.calculate95thPercentile(incomeCollection)).setScale(2);
    }

    /**
     * Calculates the median employee age by department
     *
     * @return the median employee age by department
     */
    public BigDecimal calculateMedianAge() {
        final double[] ageCollection = this.employees.stream().mapToDouble(employee -> employee.getAge().doubleValue()).toArray();
        return BigDecimal.valueOf(StatCalculator.calculateMedian(ageCollection)).setScale(2);
    }

    private double[] getAllIncomes() {
        return this.employees.stream().mapToDouble(employee -> employee.getIncome().doubleValue()).toArray();
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Department{");
        sb.append("name='").append(name).append('\'');
        sb.append(", employees=").append(employees);
        sb.append('}');
        return sb.toString();
    }

}
