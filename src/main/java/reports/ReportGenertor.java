package reports;

import model.Department;
import model.Employee;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Generates reports.
 */
public class ReportGenertor {

    private static final Logger LOGGER = Logger.getLogger(ReportGenertor.class.getName());

    /**
     * Generate reports to files.
     *
     * @param departmentList structure to generate reports from.
     * @param incomeByDep    path to income-by-department.csv - median income by department
     * @param income95ByDep  path to income-95-by-department.csv - 95-percentile income by department
     * @param incomeAvgByAge path to income-average-by-age-range.csv - average income by age ranges with factor of ten
     * @param ageByDep       path to employee-age-by-department.csv - median employee age by department
     *
     * @throws IOException in case of IO problems
     */
    public void generateReports(final List<Department> departmentList, final Path incomeByDep, final Path income95ByDep,
            final Path incomeAvgByAge, final Path ageByDep) throws IOException {

        generateReportsPerDepartment(departmentList, incomeByDep, income95ByDep, ageByDep);
        generateIncomeByAgeRangesReport(departmentList, incomeAvgByAge);
    }

    private void generateReportsPerDepartment(final List<Department> departmentList, final Path incomeByDep, final Path income95ByDep,
            final Path ageByDep) throws IOException {
        final List<CharSequence> incomeByDepFileLines = new ArrayList<>();
        final List<CharSequence> income95ByDepLines = new ArrayList<>();
        final List<CharSequence> ageByDepLines = new ArrayList<>();

        incomeByDepFileLines.add("department,medianIncome");
        income95ByDepLines.add("department,ninetyfivePercentileIncome");
        ageByDepLines.add("department,median employee age by department");

        for(final Department department : departmentList) {
            final String departmentName = department.getName();

            final BigDecimal medianIncome = department.calculateMedianIncome();
            final BigDecimal ninetyfivePercentileIncome = department.calculate95PercentileIncome();
            final BigDecimal medianAge = department.calculateMedianAge();

            LOGGER.info("Analyzing department: " + departmentName);

            incomeByDepFileLines.add(departmentName + "," + medianIncome);
            income95ByDepLines.add(departmentName + "," + ninetyfivePercentileIncome);
            ageByDepLines.add(departmentName + "," + medianAge);
        }

        Files.write(incomeByDep, incomeByDepFileLines, Charset.forName("UTF-8"), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
        Files.write(income95ByDep, income95ByDepLines, Charset.forName("UTF-8"), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
        Files.write(ageByDep, ageByDepLines, Charset.forName("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    private void generateIncomeByAgeRangesReport(final List<Department> departmentList, final Path incomeAvgByAge) throws IOException {

        final List<CharSequence> incomeAvgByAgeLines = new ArrayList<>();
        incomeAvgByAgeLines.add("ageRangeFrom,ageRangeTo,average income");

        final List<Employee> allEmployeeList =
                departmentList.stream().flatMap(department -> department.getEmployees().stream()).collect(Collectors.toList());

        final Map<Integer, List<Employee>> groupedEmployeesByAge =
                allEmployeeList.stream().collect(Collectors.groupingBy(employee -> employee.getAge() / 10));

        final List<Integer> sortedAgeRangeStart = groupedEmployeesByAge.keySet().stream().sorted().collect(Collectors.toList());

        for(final Integer ageRangeStart : sortedAgeRangeStart) {
            final List<Employee> employeesInRange = groupedEmployeesByAge.get(ageRangeStart);
            final Double sumOfIncomes = employeesInRange.stream()
                    .map(employee -> employee.getIncome().doubleValue())
                    .collect(Collectors.summingDouble(Double::doubleValue));
            final double avgIncome = sumOfIncomes / employeesInRange.size();

            incomeAvgByAgeLines.add(ageRangeStart * 10 + "," + (ageRangeStart * 10 + 9) + "," +
                    BigDecimal.valueOf(avgIncome).setScale(2, BigDecimal.ROUND_HALF_UP));
        }
        Files.write(incomeAvgByAge, incomeAvgByAgeLines, Charset.forName("UTF-8"), StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

}
