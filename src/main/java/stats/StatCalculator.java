package stats;

import java.util.Arrays;

import static java.lang.Math.floor;

/**
 * Stats calculator utility class
 */
public class StatCalculator {

    /**
     * Returns the median value.
     *
     * @param data data to calculate from
     *
     * @return the median value
     */
    public static double calculateMedian(final double[] data) {
        return calculateAtQuantile(0.5, data);
    }

    /**
     * Returns the value at the 95th percentile.
     *
     * @param data data to calculate from
     *
     * @return the value at the 95th percentile
     */
    public static double calculate95thPercentile(final double[] data) {
        return calculateAtQuantile(0.95, data);
    }

    /**
     * Returns the value at the given quantile.
     *
     * @param quantile a given quantile, in {@code [0..1]}
     * @param data     data to calculate quantile from
     *
     * @return the value in the data at {@code quantile}
     */
    private static double calculateAtQuantile(double quantile, final double[] data) {
        final double[] sortedData = Arrays.stream(data).sorted().toArray();
        if(quantile < 0.0 || quantile > 1.0 || Double.isNaN(quantile)) {
            throw new IllegalArgumentException(quantile + " is not in required range : 0.0 - 1.0");
        }

        if(sortedData.length == 0) {
            return 0.0;
        }

        final double pos = quantile * (sortedData.length + 1);
        final int index = (int) pos;

        if(index < 1) {
            return sortedData[0];
        } else if(index >= sortedData.length) {
            return sortedData[sortedData.length - 1];
        } else {
            final double lower = sortedData[index - 1];
            final double upper = sortedData[index];

            final double posDif = (pos - floor(pos));

            return lower + posDif * (upper - lower);
        }
    }
}
