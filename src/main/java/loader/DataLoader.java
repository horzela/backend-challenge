package loader;

import model.Department;
import model.Employee;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Reads the structure from input files
 */
public class DataLoader {
    public static final String SEPARATOR = ",";

    private static final Logger LOGGER = Logger.getLogger(DataLoader.class.getName());

    public List<Department> readAll(final Path depFile, final Path emplFile, final Path agesFile) {
        List<Department> departmentList = readDepartments(depFile);

        readEmployees(emplFile, departmentList);

        readAges(emplFile, agesFile, departmentList);

        return departmentList;
    }

    private List<Department> readDepartments(final Path depFile) {
        try(final Stream<String> depLines = Files.lines(depFile, Charset.forName("UTF-8"))) {
            return depLines.sorted().map(name -> new Department(name)).collect(ArrayList::new, List::add, List::addAll);
        } catch(IOException e) {
            throw new RuntimeException("Unable to readAll Departments file: " + depFile, e);
        }
    }

    private void readEmployees(final Path emplFile, final List<Department> departmentList) {
        try(final Stream<String> empLines = Files.lines(emplFile, Charset.forName("UTF-8"))) {
            empLines.forEach(line -> {
                try {
                    parseEmployee(line, departmentList);
                } catch(Exception e) {
                    // Good practice is to see the stack trace in log - adding exception itself.
                    // But since we already are not using any additional more convenient logging libs (like log4j or logback)
                    // that this is enough here just to see that we ignore corrupted data:
                    LOGGER.log(Level.WARNING,
                            "Unable to readAll data in " + emplFile + " for: " + line + " - cause: " + e.getClass() + ": " +
                                    e.getMessage());
                }
            });
        } catch(IOException e) {
            throw new RuntimeException("Unable to readAll Employees file: " + emplFile, e);
        }
    }

    private void parseAge(final Map<String, Employee> employeeIndexByName, final String line) {
        final String[] lineSplit = line.split(SEPARATOR);

        if(lineSplit.length != 2) {
            throw new IllegalArgumentException("Found corrupted data for: " + line);
        }
        final String employeeName = lineSplit[0];
        final Integer age = Integer.parseInt(lineSplit[1]);
        if(employeeIndexByName.containsKey(employeeName)) {
            employeeIndexByName.get(employeeName).setAge(age);
        } else {
            throw new IllegalArgumentException("Unable to find employee for age data: " + line);
        }
    }

    /**
     * Parses line as Employee data adding new employee to department list
     *
     * @param line           line to parse
     * @param departmentList department list which needs to be updated
     *
     * @throws IllegalArgumentException if unable to parse line properly
     */
    private void parseEmployee(final String line, final List<Department> departmentList) {
        String[] splitData = line.split(SEPARATOR);
        // check number of fields
        if(splitData.length != 4) {
            throw new IllegalArgumentException("Improper number of columns in : " + line);
        }
        final int depNr = Integer.parseInt(splitData[0]);
        final String employeeName = splitData[1];
        final BigDecimal salary = BigDecimal.valueOf(Double.parseDouble(splitData[3])).setScale(2, BigDecimal.ROUND_HALF_UP);
        final Employee employee = new Employee(employeeName, salary);

        // check if department nr is in range
        if(depNr > departmentList.size() || depNr < 1) {
            throw new IllegalArgumentException("Department nr " + depNr + " out of Departmens range ");
        }

        final Department department = departmentList.get(depNr - 1);
        department.add(employee);
    }

    private void readAges(final Path emplFile, final Path agesFile, final List<Department> departmentList) {
        final Map<String, Employee> employeeIndexByName = new HashMap<>();
        departmentList.stream()
                .flatMap(dep -> dep.getEmployees().stream())
                .forEach(employee -> employeeIndexByName.put(employee.getName(), employee));

        try(final Stream<String> ageLines = Files.lines(agesFile, Charset.forName("UTF-8"))) {
            ageLines.forEach(line -> {
                try {
                    parseAge(employeeIndexByName, line);
                } catch(Exception e) {
                    LOGGER.log(Level.WARNING,
                            "Unable to readAll data in " + agesFile + " for: " + line + " - cause: " + e.getClass() + ": " +
                                    e.getMessage());
                }

            });
        } catch(IOException e) {
            throw new RuntimeException("Unable to readAll Ages file: " + emplFile, e);
        }
    }

}
