import loader.DataLoader;
import model.Department;
import reports.ReportGenertor;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Main class.
 */
public class App {

    private final DataLoader dataLoader = new DataLoader();
    private final ReportGenertor reportGenertor = new ReportGenertor();

    public static void main(final String[] appArgs) throws IOException {
        if(appArgs.length != 3) {
            throw new IllegalArgumentException("All 3 paths must be provided");
        }

        final Path depFile = Paths.get(appArgs[0]);
        final Path emplFile = Paths.get(appArgs[1]);
        final Path agesFile = Paths.get(appArgs[2]);

        final App app = new App();
        app.run(depFile, emplFile, agesFile);
    }

    public void run(final Path depFile, final Path emplFile, final Path agesFile) throws IOException {

        final List<Department> departmentList = dataLoader.readAll(depFile, emplFile, agesFile);

        final Path outputFolder = depFile.getParent();

        reportGenertor.generateReports(departmentList, outputFolder.resolve("income-by-department.csv"),
                outputFolder.resolve("income-95-by-department.csv"), outputFolder.resolve("income-average-by-age-range.csv"),
                outputFolder.resolve("employee-age-by-department.csv"));
    }

}
